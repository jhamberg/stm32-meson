# Show commands as they are run.
set -x
# Exit on error.
set -e

ROOT=$(dirname $(realpath $0))/..

chips=('STM32F215RG' 'STM32F215VG' 'STM32F215ZG' 'STM32F205RG' 'STM32F205VG' 'STM32F205ZG' 'STM32F205RF' 'STM32F205VF' 'STM32F205ZG' 'STM32F215RE' 'STM32F215VE' 'STM32F215ZE' 'STM32F205RE' 'STM32F205VE' 'STM32F205ZE' 'STM32F205RC' 'STM32F205VC' 'STM32F205ZC' 'STM32F205RB' 'STM32F205VB' 'STM32F217VG' 'STM32F217ZG' 'STM32F217IG' 'STM32F207VG' 'STM32F207ZG' 'STM32F207IG' 'STM32F207VF' 'STM32F207ZF' 'STM32F207IF' 'STM32F217VE' 'STM32F217ZE' 'STM32F217IE' 'STM32F207VE' 'STM32F207ZE' 'STM32F207IE' 'STM32F207VC' 'STM32F207ZC' 'STM32F207IC' )


for c in ${chips[@]}
do
    echo Testing $c...
    builddir=$ROOT/build/chip-$c
    rm -rf $builddir
    mkdir -p $builddir
    meson $builddir --cross-file stm32-meson/stm32f2.build -Dstm32_chip=$c -Dstm32_cube_dir=$stm32_cube_dir
    ninja -C $builddir
done
