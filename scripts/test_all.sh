# Show commands as they are run.
set -x
# Exit on error.
set -e

stm32_cube_dir=$(realpath ~/STM32Cube/Repository/STM32Cube_FW_F3_V1.11.0/)
sh scripts/test_f3.sh

stm32_cube_dir=$(realpath ~/STM32Cube/Repository/STM32Cube_FW_F1_V1.8.0)
sh scripts/test_f1.sh

stm32_cube_dir=$(realpath ~/STM32Cube/Repository/STM32Cube_FW_F0_V1.11.0)
sh scripts/test_f0.sh

stm32_cube_dir=$(realpath ~/STM32Cube/Repository/STM32Cube_FW_F2_V1.8.0)
sh scripts/test_f2.sh

stm32_cube_dir=$(realpath ~/STM32Cube/Repository/STM32Cube_FW_F4_V1.24.2)
sh scripts/test_f4.sh

stm32_cube_dir=$(realpath ~/STM32Cube/Repository/STM32Cube_FW_G0_V1.3.0)
sh scripts/test_g0.sh


# stm32_cube_dir=$(realpath ~/STM32Cube/Repository/STM32Cube_FW_G0_V1.3.0)
# sh scripts/test_g0.sh

