# About
This project is used to develop applications for ST's ARM Cortex-Mx MCUs using meson and GCC.
This project is inspired by [stm32-cmake](https://github.com/ObKo/stm32-cmake).

# Examples
* stm32-blinky - blink LED using times and PWM
# Configuration
* stm32_chip - STM32 device code, e.g. STM32G071RB, STM32f103VG
* stm32_cube_dir - path to STM32CubeMX directory
* --cross-file - contains build and linker flags required to compile the project

# Common Usage
```sh
# Create build files.
meson build --cross-file stm32-meson/stm32.build -Dstm32_chip=STM32G071RB -Dstm32_cube_dir=$HOME/STM32Cube/Repository/STM32Cube_FW_G0_V1.3.0

# Build the code.
ninja -C build
```
Important variables.

Input Variables

* stm32_chip - The chip the target is being compiled against. Eg. STM32G071RB
* stm32_hal_components Components required for the project. Eg. tim, pwr, rcc

Output Variables

* stm32_family - The family extracted from the stm32_chip variable. Eg. STM32G0

Output Dependencies

* stm32_cubemx_dep - Contains cube_mx hal dependencies and linker script.
* stm32_cmsis_dep - Contains CMSIS headers and system startup code.

# Supported Families
STM32G0 Family
